require 'spec_helper'

describe MeetingService do
  let(:first_company)  { FactoryGirl.create :company }
  let(:second_company) { FactoryGirl.create :company }
  let(:first_block_spots) { Meeting.spots(:first_block) }
  let(:second_block_spots) { Meeting.spots(:second_block) }
  let(:third_block_spots) { Meeting.spots(:third_block) }
  let(:all_spots) { first_block_spots + second_block_spots + third_block_spots }

  context '#common_available_spots' do
    let(:available_spots) { MeetingService.common_available_spots first_company, second_company }

    it 'returns all spots when none of the companies have meetings' do
      expect(available_spots).to eq(all_spots)
    end

    it 'returns none if the first company meeting spots are full' do
      all_spots.each do |spot|
        company = FactoryGirl.create :company
        Meeting.create requester_company: first_company, company: company, begin_at: spot[:begin_at], end_at: spot[:end_at]
      end

      expect(available_spots).to be_empty
    end

    it 'returns none if the second company meeting spots are full' do
      all_spots.each do |spot|
        company = FactoryGirl.create :company
        Meeting.create requester_company: second_company, company: company, begin_at: spot[:begin_at], end_at: spot[:end_at]
      end

      expect(available_spots).to be_empty
    end

    it 'return second and third block spots when first company has all first block booked' do
      first_block_spots.each do |spot|
        company = FactoryGirl.create :company
        Meeting.create requester_company: first_company, company: company, begin_at: spot[:begin_at], end_at: spot[:end_at]
      end

      expect(available_spots).to eq(second_block_spots + third_block_spots)
    end

    it 'return second and third block spots when second company has all first block booked' do
      first_block_spots.each do |spot|
        company = FactoryGirl.create :company
        Meeting.create requester_company: second_company, company: company, begin_at: spot[:begin_at], end_at: spot[:end_at]
      end

      expect(available_spots).to eq(second_block_spots + third_block_spots)
    end

    it 'return second block spots when first company has all first block booked and second has all third block booked' do
      first_block_spots.each do |spot|
        company = FactoryGirl.create :company
        Meeting.create requester_company: first_company, company: company, begin_at: spot[:begin_at], end_at: spot[:end_at]
      end

      third_block_spots.each do |spot|
        company = FactoryGirl.create :company
        Meeting.create requester_company: second_company, company: company, begin_at: spot[:begin_at], end_at: spot[:end_at]
      end

      expect(available_spots).to eq(second_block_spots)
    end
  end

  it '#all_tables' do
    expect(MeetingService.all_tables).to eq((1..47).to_a)
  end

  it '#available_tables_for' do
    begin_at = Time.parse("2000-01-01 09:00:00 UTC")
    end_at   = begin_at + 15.minutes
    spot = { begin_at: begin_at, end_at: end_at }

    MeetingService.all_tables.drop(10).each do |table_number|
      company1 = FactoryGirl.create :company
      company2 = FactoryGirl.create :company

      Meeting.create requester_company: company1, company: company2,
        begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: table_number
    end

    expect(MeetingService.available_tables_for(spot)).to eq(MeetingService.all_tables.take(10))
  end

  context '#arrange_meeting' do
    let(:arranged_meeting) { MeetingService.arrange_meeting(first_company, second_company) }
    let(:begin_at) { Time.parse("2000-01-01 09:00:00 UTC") }
    let(:end_at) { begin_at + 15.minutes }
    let(:spot) { { begin_at: begin_at, end_at: end_at } }
    let(:meeting) do
      meeting = Meeting.new requester_company: first_company, company: second_company
      meeting.begin_at = spot[:begin_at]
      meeting.end_at   = spot[:end_at]
      meeting.table_number = 1
      meeting
    end

    it 'returns first spot on first table if no company have meetings' do
      expect(arranged_meeting.attributes).to eq(meeting.attributes)
    end

    it 'returns first spot on lowest numbered table if no company have meetings and initial tables are taken' do
      MeetingService.all_tables.take(9).each do |table_number|
        company1 = FactoryGirl.create :company
        company2 = FactoryGirl.create :company
        Meeting.create requester_company: company1, company: company2,
          begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: table_number
      end

      meeting.table_number = 10

      expect(arranged_meeting.attributes).to eq(meeting.attributes)
    end

    it 'returns first common spot on lowest numbered table if no company have meetings and initial tables are taken' do
      second_block_spots.each do |spot|
        MeetingService.all_tables.take(9).each do |table_number|
          company1 = FactoryGirl.create :company
          company2 = FactoryGirl.create :company
          Meeting.create requester_company: company1, company: company2,
            begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: table_number
        end
      end

      first_block_spots.each do |spot|
        company = FactoryGirl.create :company
        Meeting.create requester_company: first_company, company: company,
          begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: 1
      end

      meeting.begin_at = "11:30:00"
      meeting.end_at   = meeting.begin_at + 15.minutes
      meeting.table_number = 10

      expect(arranged_meeting.attributes).to eq(meeting.attributes)
    end

    it 'returns earliest common available spot with a free table' do
      second_block_spots.each do |spot|
        MeetingService.all_tables.take(9).each do |table_number|
          company1 = FactoryGirl.create :company
          company2 = FactoryGirl.create :company
          Meeting.create requester_company: company1, company: company2,
            begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: table_number
        end
      end

      second_block_spots.take(2).each do |spot|
        MeetingService.all_tables.drop(9).each do |table_number|
          company1 = FactoryGirl.create :company
          company2 = FactoryGirl.create :company
          Meeting.create requester_company: company1, company: company2,
            begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: table_number
        end
      end

      first_block_spots.each do |spot|
        company = FactoryGirl.create :company
        Meeting.create requester_company: first_company, company: company,
          begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: 1
      end

      meeting.begin_at = "12:10:00"
      meeting.end_at   = meeting.begin_at + 15.minutes
      meeting.table_number = 10

      expect(arranged_meeting.attributes).to eq(meeting.attributes)
    end

    it 'returns nil when there are no free tables on any common available spot' do
      second_block_spots.each do |spot|
        MeetingService.all_tables.each do |table_number|
          company1 = FactoryGirl.create :company
          company2 = FactoryGirl.create :company
          Meeting.create requester_company: company1, company: company2,
            begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: table_number
        end
      end

     third_block_spots.each do |spot|
        MeetingService.all_tables.each do |table_number|
          company1 = FactoryGirl.create :company
          company2 = FactoryGirl.create :company
          Meeting.create requester_company: company1, company: company2,
            begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: table_number
        end
      end

      first_block_spots.each do |spot|
        company = FactoryGirl.create :company
        Meeting.create requester_company: first_company, company: company,
          begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: 1
      end

      expect(arranged_meeting).to be_nil
    end
  end

  context '#can_request_meeting' do
    let(:begin_at) { Time.parse("2000-01-01 09:00:00 UTC") }
    let(:end_at) { begin_at + 15.minutes }
    let(:spot) { { begin_at: begin_at, end_at: end_at } }

    context 'a meeting already exists' do
      it 'returns false if requested by first company' do
        Meeting.create requester_company: first_company, company: second_company,
          begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: 1

        expect(MeetingService.can_request_meeting(first_company, second_company)).to be_false
      end

      it 'returns false if requested by second company' do
        Meeting.create requester_company: second_company, company: first_company,
          begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: 1

        expect(MeetingService.can_request_meeting(first_company, second_company)).to be_false
      end
    end

    context 'a meeting request already exists' do
      it 'returns false if requested by first company' do
        MeetingRequest.create requester_company: first_company, requestee_company: second_company
        expect(MeetingService.can_request_meeting(first_company, second_company)).to be_false
      end

      it 'returns false if requested by second company' do
        MeetingRequest.create requester_company: second_company, requestee_company: first_company
        expect(MeetingService.can_request_meeting(first_company, second_company)).to be_false
      end
    end

    it 'returns false if both companies are the same' do
      expect(MeetingService.can_request_meeting(first_company, first_company)).to be_false
    end

    it 'returns false if requester has no available meeting spots' do
      all_spots.each do |spot|
        company = FactoryGirl.create :company
        Meeting.create requester_company: first_company, company: company, begin_at: spot[:begin_at], end_at: spot[:end_at]
      end

      expect(MeetingService.can_request_meeting(first_company, second_company)).to be_false
    end

    it 'returns false if requestee has no available meeting spots' do
      all_spots.each do |spot|
        company = FactoryGirl.create :company
        Meeting.create requester_company: second_company, company: company, begin_at: spot[:begin_at], end_at: spot[:end_at]
      end

      expect(MeetingService.can_request_meeting(first_company, second_company)).to be_false
    end

    it 'returns false if requester has made more than half the total possible requests' do
      all_spots.drop(all_spots.count / 2).each_index do |index|
        company = FactoryGirl.create :company
        MeetingRequest.create! requester_company: first_company, requestee_company: company,
          status: (index % 2 == 0 ? :pending : :scheduled)
      end

      expect(MeetingService.can_request_meeting(first_company, second_company)).to be_false
    end

    it 'returns false if requestee has received more than half the total possible requests' do
      all_spots.drop(all_spots.count / 2).each_index do |index|
        company = FactoryGirl.create :company
        MeetingRequest.create! requester_company: company, requestee_company: second_company,
          status: (index % 2 == 0 ? :pending : :scheduled)
      end

      expect(MeetingService.can_request_meeting(first_company, second_company)).to be_false
    end

    it 'returns false if requester has made more than "outgoing_request_limit" requests' do
      first_company.update outgoing_request_limit: 4

      4.times do |index|
        company = FactoryGirl.create :company
        MeetingRequest.create! requester_company: first_company, requestee_company: company,
          status: (index % 2 == 0 ? :pending : :scheduled)
      end

      expect(MeetingService.can_request_meeting(first_company, second_company)).to be_false
    end

    it 'returns false if requestee has received more than "incoming_request_limit" requests' do
      second_company.update incoming_request_limit: 4

      4.times do |index|
        company = FactoryGirl.create :company
        MeetingRequest.create! requester_company: company, requestee_company: second_company,
          status: (index % 2 == 0 ? :pending : :scheduled)
      end

      expect(MeetingService.can_request_meeting(first_company, second_company)).to be_false
    end

    it 'returns false when time limit is reached' do
      Timecop.freeze(Time.parse("2013-07-01 15:01:00 -0430"))

      expect(MeetingService.can_request_meeting(first_company, second_company)).to be_false
    end

    it 'returns true when time limit is reached but is overriden by company' do
      Timecop.freeze(Time.parse("2013-07-01 15:01:00 -0430"))

      first_company.update time_limit: Time.parse("2013-07-01 15:30:00 -0430")

      expect(MeetingService.can_request_meeting(first_company, second_company)).to be_true
    end
  end

  context '#can_accept_meeting_request' do
    let(:requester_company)   { FactoryGirl.create :company }
    let(:requestee_company)  { FactoryGirl.create :company }
    let(:meeting_request) do
      MeetingRequest.create requester_company: requester_company, requestee_company: requestee_company, status: :unknown
    end

    it 'cannot be accepted by requester' do
      meeting_request.update status: :pending
      expect(MeetingService.can_accept_meeting_request(meeting_request, requester_company)).to be_false
    end

    it 'can only be accepted by requestee when pending' do
      meeting_request.update status: :pending
      expect(MeetingService.can_accept_meeting_request(meeting_request, requestee_company)).to be_true
    end

    it 'cannot be accepted if not pending' do
      meeting_request.update status: :rejected
      expect(MeetingService.can_accept_meeting_request(meeting_request, requestee_company)).to be_false
    end
  end

  context '#can_reject_meeting_request' do
    let(:requester_company)  { FactoryGirl.create :company }
    let(:requestee_company)  { FactoryGirl.create :company }
    let(:meeting_request) do
      MeetingRequest.create requester_company: requester_company, requestee_company: requestee_company, status: :unknown
    end

    it 'cannot be rejected by requester' do
      meeting_request.update status: :pending
      expect(MeetingService.can_reject_meeting_request(meeting_request, requester_company)).to be_false
    end

    it 'can only be reject by requestee when pending' do
      meeting_request.update status: :pending
      expect(MeetingService.can_reject_meeting_request(meeting_request, requestee_company)).to be_true
    end

    it 'cannot be reject if not pending' do
      meeting_request.update status: :accepted
      expect(MeetingService.can_reject_meeting_request(meeting_request, requestee_company)).to be_false
    end
  end
end

require 'spec_helper'

describe Company do
  context '.has_meeting_with' do
    let(:company) { FactoryGirl.create :company }
    let(:other_company) { FactoryGirl.create :company }

    it 'returns false when there are no meetings' do
      expect(company.has_meeting_with(other_company)).to be_false
    end

    it 'returns true when is requester' do
      Meeting.create requester_company: company, company: other_company
      expect(company.has_meeting_with(other_company)).to be_true
    end

    it 'returns true when is requestee' do
      Meeting.create requester_company: other_company, company: company
      expect(company.has_meeting_with(other_company)).to be_true
    end
  end

  context '.all_available_meeting_spots' do
    let(:company) { FactoryGirl.create :company }

    it 'returns 0 when there are meetings on every spot' do
      Meeting.all_spots.each do |spot|
        any_company = FactoryGirl.create :company
        Meeting.create! requester_company: company, company: any_company,
          begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: 4
      end

      expect(company.all_available_meeting_spots.count).to be(0)
    end

    it 'returns the number of free spots when there just a few scheduled meetings' do
      booked_spots = Meeting.all_spots.sample(4)
      booked_spots.each do |spot|
        any_company = FactoryGirl.create :company
        Meeting.create! requester_company: company, company: any_company,
          begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: 4
      end

      expect(company.all_available_meeting_spots.count).to be(Meeting.all_spots.count - 4)
    end
  end
end

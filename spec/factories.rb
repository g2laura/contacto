FactoryGirl.define do
  factory :company do
    sequence(:name) { |n| "Company #{n}" }
  end

  factory :user do
    sequence(:email) { |n| "john.doe#{n}@example.com" }
    password '12345678'
    company
  end
end

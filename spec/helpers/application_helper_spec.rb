require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the ApplicationHelper. For example:
#
# describe ApplicationHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       helper.concat_strings("this","that").should == "this that"
#     end
#   end
# end
describe ApplicationHelper do
  before do
    Timecop.freeze(Time.parse("2013-07-01 00:00:00"))
  end

  after do
    Timecop.return
  end

  context '.display_new_meeting_request_link_for?' do
    include Devise::TestHelpers

    let(:company) { FactoryGirl.create :company }
    let(:other_company) { FactoryGirl.create :company }
    let(:user) { FactoryGirl.create :user, company: company }

    it 'returns true if there are no scheduled meeting' do
      sign_in user
      expect(helper.display_new_meeting_request_link_for? other_company).to be_true
    end

    it 'returns false when there is a meeting requested by the company' do
      Meeting.create! requester_company: company, company: other_company
      sign_in user
      expect(helper.display_new_meeting_request_link_for? other_company).to be_false
    end

    it 'returns false when there is a meeting requested by the other company' do
      Meeting.create! requester_company: company, company: other_company
      sign_in user
      expect(helper.display_new_meeting_request_link_for? other_company).to be_false
    end

    it 'returns false when there are no available spots' do
      Meeting.all_spots.each do |spot|
        any_company = FactoryGirl.create :company
        Meeting.create! requester_company: company, company: any_company,
          begin_at: spot[:begin_at], end_at: spot[:end_at], table_number: 4
      end
      sign_in user
      expect(helper.display_new_meeting_request_link_for? other_company).to be_false
    end

    it 'returns false when time limit is reached' do
      Timecop.freeze(Time.parse("2013-07-01 15:01:00 -0430"))

      sign_in user
      expect(helper.display_new_meeting_request_link_for? other_company).to be_false
    end

    it 'returns true when time limit is reached but is overriden by company' do
      Timecop.freeze(Time.parse("2013-07-01 15:01:00 -0430"))

      company.update time_limit: Time.parse("2013-07-01 15:30:00 -0430")

      sign_in user
      expect(helper.display_new_meeting_request_link_for? other_company).to be_true
    end
  end
end

namespace :import do
  task :excel => :environment do |t, args|
    verbose = ENV["VERBOSE"] == "true" ? true : false

    file = ARGV[1]
    puts "importing from excel file: #{file}"

    Rails.logger = Logger.new(STDOUT)
    Rails.logger.level = Logger::INFO unless verbose
    ExcelImportService.new(file).import
  end
end

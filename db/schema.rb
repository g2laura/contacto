# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140122191901) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answers", force: true do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.integer  "possible_answer_id"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "answers", ["possible_answer_id"], name: "index_answers_on_possible_answer_id", using: :btree
  add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree
  add_index "answers", ["user_id"], name: "index_answers_on_user_id", using: :btree

  create_table "companies", force: true do |t|
    t.string   "name"
    t.text     "summary"
    t.text     "services"
    t.text     "needs"
    t.text     "offers"
    t.string   "logo"
    t.integer  "sector_id"
    t.integer  "contact_person_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "sponsor",                default: false
    t.string   "website"
    t.string   "twitter"
    t.string   "phone_number"
    t.string   "external_id"
    t.boolean  "anchor",                 default: false
    t.integer  "incoming_request_limit", default: -1,    null: false
    t.integer  "outgoing_request_limit", default: -1,    null: false
    t.datetime "time_limit"
  end

  add_index "companies", ["contact_person_id"], name: "index_companies_on_contact_person_id", using: :btree
  add_index "companies", ["sector_id"], name: "index_companies_on_sector_id", using: :btree

  create_table "meeting_requests", force: true do |t|
    t.integer  "requester_company_id"
    t.integer  "requestee_company_id"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "meeting_requests", ["requestee_company_id"], name: "index_meeting_requests_on_requestee_company_id", using: :btree
  add_index "meeting_requests", ["requester_company_id"], name: "index_meeting_requests_on_requester_company_id", using: :btree

  create_table "meetings", force: true do |t|
    t.integer  "status"
    t.time     "begin_at"
    t.time     "end_at"
    t.integer  "requester_company_id"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "table_number"
  end

  add_index "meetings", ["company_id"], name: "index_meetings_on_company_id", using: :btree
  add_index "meetings", ["requester_company_id"], name: "index_meetings_on_requester_company_id", using: :btree

  create_table "possible_answers", force: true do |t|
    t.integer  "question_id"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "explain"
    t.boolean  "option"
  end

  add_index "possible_answers", ["question_id"], name: "index_possible_answers_on_question_id", using: :btree

  create_table "questions", force: true do |t|
    t.string   "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "survey_id"
  end

  add_index "questions", ["survey_id"], name: "index_questions_on_survey_id", using: :btree

  create_table "sectors", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "surveys", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone_number"
    t.integer  "company_id"
    t.boolean  "admin",                  default: false
    t.string   "mobile_phone"
    t.string   "external_id"
  end

  add_index "users", ["company_id"], name: "index_users_on_company_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end

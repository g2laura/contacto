# create admin user
User.where(email: 'admin@local.host').first_or_create do |user|
  user.password = '12345678'
  user.admin = true
end

if Survey.first.nil?
  survey = Survey.create do |s|
    s.name = 'Encuesta'
  end
  Question.create do |q|
    q.text = 'De los eventos programados ¿todos se concretaron?'
    PossibleAnswer.create do |pa|
      pa.text = 'Si'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = 'No (señale cuales no se realizaron)'
      pa.question = q
      pa.explain = true
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = '¿Durante el evento tuvo encuentros no programados con 
representantes de otras organizaciones?'
    PossibleAnswer.create do |pa|
      pa.text = 'Si (señale cuales)'
      pa.question = q
      pa.explain = true
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = 'No'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'Una vez finalizado el encuentro, estableció contacto (vía correo 
electrónico o telefónico) con algunas de las organizaciones asistentes:'
    PossibleAnswer.create do |pa|
      pa.text = 'Si (señale cuales)'
      pa.question = q
      pa.explain = true
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = 'No'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'Una vez finalizado el encuentro, alguna de las organizaciones asistentes 
estableció contacto (vía correo electrónico o telefónico) con su organización:'
    PossibleAnswer.create do |pa|
      pa.text = 'Si (señale cuales)'
      pa.question = q
      pa.explain = true
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = 'No'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'Una vez finalizado el encuentro se concretaron reuniones con una o 
varias de las organizaciones asistente'
    PossibleAnswer.create do |pa|
      pa.text = 'Si (señale cuales)'
      pa.question = q
      pa.explain = true
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = 'No'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'De los contactos establecidos con otras organizaciones asistentes, se ha 
concretado algún tipo de alianza comercial o social'
    PossibleAnswer.create do |pa|
      pa.text = 'Si (señale con cuál organización por tipo de alianza)'
      pa.question = q
      pa.explain = false
      pa.option = false
    end
    PossibleAnswer.create do |pa|
      pa.text = 'Comercial (intercambio de bienes o servicios, por ejemplo)'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = 'Social (Alianza para el apoyo a una ONG o proyecto 
de Responsabilidad Social, por ejemplo)'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = 'No, pero está en negociación (señale con cuál)'
      pa.question = q
      pa.explain = false
      pa.option = false
    end
    PossibleAnswer.create do |pa|
      pa.text = 'Comercial (intercambio de bienes o servicios, por ejemplo)'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = 'Social (Alianza para el apoyo a una ONG o proyecto 
de Responsabilidad Social, por ejemplo)'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = 'No se concretó'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'Señale, por orden de preferencia, ¿cuáles considera que son los factores 
que según su opinión favorecen esta alianza (concretada o en proceso 
de negociación)?'
  end

  Question.create do |q|
    q.text = 'Mi interlocutor me genera confianza'
    PossibleAnswer.create do |pa|
      pa.text = '1'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '2'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '3'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '4'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '5'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '6'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '7'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '8'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'Ambas organizaciones estamos dispuestos a cooperar'
    PossibleAnswer.create do |pa|
      pa.text = '1'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '2'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '3'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '4'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '5'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '6'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '7'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '8'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'Ambas organizaciones poseemos intereses comunes'
    PossibleAnswer.create do |pa|
      pa.text = '1'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '2'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '3'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '4'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '5'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '6'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '7'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '8'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'Ambas organizaciones estamos dispuestos a asociarnos'
    PossibleAnswer.create do |pa|
      pa.text = '1'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '2'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '3'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '4'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '5'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '6'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '7'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '8'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'Ambas organizaciones manifestamos actitudes positivas hacia el cumplimiento de la meta'
    PossibleAnswer.create do |pa|
      pa.text = '1'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '2'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '3'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '4'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '5'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '6'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '7'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '8'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'Ambas organizaciones manejamos transparentemente la relación'
    PossibleAnswer.create do |pa|
      pa.text = '1'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '2'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '3'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '4'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '5'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '6'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '7'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '8'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'Ambas organizaciones estamos dispuestos a innovar'
    PossibleAnswer.create do |pa|
      pa.text = '1'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '2'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '3'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '4'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '5'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '6'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '7'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '8'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'Mi interlocutor posee liderazgo'
    PossibleAnswer.create do |pa|
      pa.text = '1'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '2'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '3'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '4'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '5'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '6'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '7'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = '8'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'Una vez finalizado el encuentro y hasta la fecha, ¿su organización 
colaboró para que otra organización asistente de CONTACTOve se relacionara 
con otras organizaciones o personas que no asistieron?'
    PossibleAnswer.create do |pa|
      pa.text = 'No'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = 'Si. Detalle, hasta 3 casos reemplazando las frases entre paréntesis, 
      cada una con el siguiente formato: Presenté a (Nombre organización asistente) con (Nombre organización / 
      persona no asistente con la que la relacionó)'
      pa.question = q
      pa.explain = true
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = 'Una vez finalizado el encuentro y hasta la fecha, ¿otra organización 
asistente colaboró para que se estableciera una relación entre su 
organización y otra organización / persona no asistente al evento?'
    PossibleAnswer.create do |pa|
      pa.text = 'No'
      pa.question = q
      pa.explain = false
      pa.option = true
    end
    PossibleAnswer.create do |pa|
      pa.text = 'Si. Detalle, hasta 3 casos reemplazando las frases entre paréntesis, 
      cada una con el siguiente formato: (Nombre organización asistente) me presentó (Nombre organización / 
      persona no asistente con la que la relacionó)'
      pa.question = q
      pa.explain = true
      pa.option = true
    end
  end

  Question.create do |q|
    q.text = '¿Con cuál de las organizaciones asistentes con las que NO estableció 
contacto o alianza, le hubiese interesado concretar algún tipo de 
sociedad? (señale con cuál)'
    PossibleAnswer.create do |pa|
      pa.text = ''
      pa.question = q
      pa.explain = true
      pa.option = false
    end
  end

  Question.create do |q|
    q.text = 'PREGUNTA ABIERTA: Para finalizar, ¿Qué organización o tipo de 
organización no asistente al evento, le interesaría conocer en la próxima 
edición de Contacto?'
    PossibleAnswer.create do |pa|
      pa.text = ''
      pa.question = q
      pa.explain = true
      pa.option = false
    end
  end

  Question.all.each do |q|
    q.survey_id = survey.id
    q.save
  end
end

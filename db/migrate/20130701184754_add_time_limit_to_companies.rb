class AddTimeLimitToCompanies < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.datetime :time_limit
    end
  end
end

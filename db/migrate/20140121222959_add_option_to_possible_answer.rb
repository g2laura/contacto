class AddOptionToPossibleAnswer < ActiveRecord::Migration
  def change
    add_column :possible_answers, :option, :boolean
  end
end

class AddMobilePhoneToUser < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :mobile_phone
    end
  end
end

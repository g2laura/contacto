class AddPhoneNumberAndExternalIdToCompanies < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.string :phone_number
      t.string :external_id
    end
  end
end

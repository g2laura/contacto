class AddMaxIncomingAndOutgoingRequestToCompanies < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.integer :incoming_request_limit, null: false, default: -1
      t.integer :outgoing_request_limit, null: false, default: -1
    end
  end
end

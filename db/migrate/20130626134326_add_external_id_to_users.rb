class AddExternalIdToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :external_id
    end
  end
end

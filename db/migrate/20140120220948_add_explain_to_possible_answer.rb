class AddExplainToPossibleAnswer < ActiveRecord::Migration
  def change
    add_column :possible_answers, :explain, :boolean
  end
end

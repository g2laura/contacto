class ChangeBeginAndEndToTimeOnMeetings < ActiveRecord::Migration
  def change
    change_column :meetings, :begin_at, :time, index: true
    change_column :meetings, :end_at, :time, index: true
  end
end

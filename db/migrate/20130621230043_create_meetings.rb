class CreateMeetings < ActiveRecord::Migration
  def change
    create_table :meetings do |t|
      t.integer :status
      t.datetime :begin_at
      t.datetime :end_at
      t.references :requester_company, index: true
      t.references :company, index: true

      t.timestamps
    end
  end
end

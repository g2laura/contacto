class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.text   :summary
      t.text   :services
      t.text   :needs
      t.text   :offers
      t.string :logo
      t.references :sector, index: true
      t.references :contact_person, index: true

      t.timestamps
    end
  end
end

class AddSponsorSwitchToCompanies < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.boolean :sponsor, default: false, index: true
    end
  end
end

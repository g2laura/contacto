class AddTableNumberToMeetings < ActiveRecord::Migration
  def change
    change_table :meetings do |t|
      t.integer :table_number
    end
  end
end

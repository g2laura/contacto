class AddAdminSwitchToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.boolean :admin, default: false, index: true
    end
  end
end

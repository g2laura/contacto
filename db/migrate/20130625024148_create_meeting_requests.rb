class CreateMeetingRequests < ActiveRecord::Migration
  def change
    create_table :meeting_requests do |t|
      t.references :requester_company, index: true
      t.references :requestee_company, index: true
      t.string :status

      t.timestamps
    end
  end
end

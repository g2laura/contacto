class AddSurveyRefToQuestions < ActiveRecord::Migration
  def change
    add_reference :questions, :survey, index: true
  end
end

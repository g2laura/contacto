class AddAnchorSwitchToCompanies < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.boolean :anchor, default: false
    end
  end
end

class AddPersonalInformationToUser < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone_number
      t.references :company, index: true
    end
  end
end

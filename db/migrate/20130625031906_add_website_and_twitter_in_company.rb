class AddWebsiteAndTwitterInCompany < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.string :website
      t.string :twitter
    end
  end
end
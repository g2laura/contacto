class MeetingsController < ApplicationController

  def index
    @company = current_user.company
    @meetings = @company.meetings

    respond_to do |format|
      format.html
      format.pdf do
        pdf = ScheduleDocument.new(@company)
        send_data pdf.render, filename: 'contacto-ve-agenda.pdf',
                              type: 'application/pdf',
                              disposition: 'inline'
      end
    end
  end

  private
    def meeting_params
      params[:meeting]
    end
end

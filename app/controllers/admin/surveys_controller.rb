module Admin
  class SurveysController < AdministrationController
    before_action :set_survey, only: [:show, :edit, :update, :destroy]

    # GET /surveys
    # GET /surveys.json
    def index
      @survey  = Survey.first
      @count = 1
    end

    def companies
      @survey  = Survey.first
      @users = User.includes(:answers).where("answers is not null").references(:answers)
    end

    def users
      @users = User.includes(:answers).where("answers is not null").references(:answers)
    end

    def user
      @survey  = Survey.first
      @user = User.find(params[:id])
    end

    # GET /surveys/new
    def new
      @survey = Survey.new
    end

    # GET /surveys/1/edit
    def edit
    end

    # POST /surveys
    # POST /surveys.json
    def create
      @survey = Survey.new(survey_params)

      respond_to do |format|
        if @survey.save
          format.html { redirect_to surveys_path, notice: 'Survey was successfully created.' }
          format.json { render action: 'show', status: :created, location: @survey }
        else
          format.html { render action: 'new' }
          format.json { render json: @survey.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /surveys/1
    # PATCH/PUT /surveys/1.json
    def update
      respond_to do |format|
        if @survey.update(survey_params)
          format.html { redirect_to surveys_path, notice: 'Survey was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @survey.errors, status: :unprocessable_entity }
        end
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_survey
        @survey = Survey.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def survey_params
        params.require(:survey).permit(:name)
        params.require(:survey).permit(:name,
          :questions_attributes => [:id, :name,
            :answers_attributes => [:id, :user_id, :question_id, :possible_answer_id, :text]
          ])
      end
    end
end

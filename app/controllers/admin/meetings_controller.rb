module Admin
  class MeetingsController < AdministrationController
    def index
      @all_tables = (1..Meeting::MAX_TABLE_NUMBER).to_a
      @tables = Kaminari.paginate_array(@all_tables).page(params[:page]).per(5)

      @meetings = Meeting.on_table(@tables)

      respond_to do |format|
        format.html
        format.pdf do
          pdf = AssignmentDocument.new
          send_data pdf.render, filename: 'contacto-ve-table-assignment.pdf',
                                type: 'application/pdf',
                                disposition: 'inline'
        end
      end
    end
  end
end

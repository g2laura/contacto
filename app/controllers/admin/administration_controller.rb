module Admin
  class AdministrationController < ApplicationController
    before_action :authorize_admin!

    protected
    def authorize_admin!
      redirect_to root_path unless current_user.admin?
    end
  end
end

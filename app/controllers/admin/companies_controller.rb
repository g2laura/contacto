module Admin
  class CompaniesController < AdministrationController
    before_action :set_company, only: [:show, :edit, :update, :destroy, :schedule, :send_schedule]

    # GET /companies
    # GET /companies.json
    def index
      @companies ||= Company.all
      @companies = @companies.search params[:search] if params[:search] && !params[:search].empty?

      @companies = @companies.page(params[:page])

      render 'index'
    end

    def sponsors
      @companies = Company.sponsors
      index
    end

    def anchors
      @companies = Company.anchors
      index
    end

    # GET /companies/1
    # GET /companies/1.json
    def show
    end

    def schedule
      respond_to do |format|
        format.pdf do
          pdf = ScheduleDocument.new(@company)
          send_data pdf.render, filename: 'contacto-ve-agenda.pdf',
                                type: 'application/pdf',
                                disposition: 'inline'
        end
      end
    end

    def send_schedule
      MeetingRequestMailer.pdf_schedule(@company.id).deliver
    end

    # GET /companies/new
    def new
      @company = Company.new
    end

    # GET /companies/1/edit
    def edit
    end

    # POST /companies
    # POST /companies.json
    def create
      @company = Company.new(company_params)

      respond_to do |format|
        if @company.save
          format.html { redirect_to [:admin, @company], notice: 'Admin company was successfully created.' }
        else
          format.html { render action: 'new' }
        end
      end
    end

    # PATCH/PUT /companies/1
    # PATCH/PUT /companies/1.json
    def update
      respond_to do |format|
        if @company.update(company_params)
          format.html { redirect_to [:admin, @company], notice: 'Admin company was successfully updated.' }
        else
          format.html { render action: 'edit' }
        end
      end
    end

    # DELETE /companies/1
    # DELETE /companies/1.json
    def destroy
      @company.destroy
      respond_to do |format|
        format.html { redirect_to admin_companies_url }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_company
        @company = Company.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def company_params
        params.require(:company).permit(:external_id, :logo, :name, :sector_id, :contact_person_id, :website, :twitter,
          :phone_number, :summary, :services, :needs, :offers, :sponsor, :anchor, :incoming_request_limit, :outgoing_request_limit, :time_limit) if params[:query].nil?
      end
  end
end

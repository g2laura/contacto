module Admin
  class MeetingRequestsController < ApplicationController
    before_action :set_meeting_request, only: [:destroy, :notify]

    def notify
      case @meeting_request.status
      when :pending
        MeetingRequestMailer.created(@meeting_request.id).deliver

        redirect_to :back, notice: I18n.t('admin.meeting_requests.notify.created_notification_succesfully_sent')
      when :scheduled
        meeting = @meeting_request.meeting

        if meeting
          [:requester_company, :company].each do |company_kind|
            MeetingRequestMailer.scheduled(meeting.id, meeting.send(company_kind).id).deliver
          end
        end

        redirect_to :back, notice: I18n.t('admin.meeting_requests.notify.scheduled_notification_succesfully_sent')
      end
    end

    def destroy
      if @meeting_request
        if @meeting_request.meeting
          @meeting_request.meeting.delete
        end

        @meeting_request.delete
      end

      redirect_to :back, notice: I18n.t('admin.meeting_requests.notify.destroyed')
    end

    private
      def set_meeting_request
        @meeting_request = MeetingRequest.find(params[:id])
      end
  end
end

module Admin
  class UsersController < AdministrationController
    before_action :set_user, only: [:show, :edit, :update, :destroy]

    # GET /users
    # GET /users.json
    def index
      @users = User.all
    end

    def admins
      @users = User.admins
      render action: 'index'
    end

    def sponsors
      @users = User.sponsors
      render action: 'index'
    end

    def anchors
      @users = User.anchors
      render action: 'index'
    end

    # GET /users/1
    # GET /users/1.json
    def show
    end

    # GET /users/new
    def new
      @user = User.new
    end

    # GET /users/1/edit
    def edit
    end

    # POST /users
    # POST /users.json
    def create
      @user = User.new(user_params)

      respond_to do |format|
        if @user.save
          format.html { redirect_to [:admin, @user], notice: 'User was successfully created.' }
        else
          format.html { render action: 'new' }
        end
      end
    end

    # PATCH/PUT /users/1
    # PATCH/PUT /users/1.json
    def update
      respond_to do |format|
        if @user.update(user_params)
          format.html { redirect_to [:admin, @user], notice: 'User was successfully updated.' }
        else
          format.html { render action: 'edit' }
        end
      end
    end

    # DELETE /users/1
    # DELETE /users/1.json
    def destroy
      @user.destroy
      respond_to do |format|
        format.html { redirect_to admin_users_url }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_user
        @user = User.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def user_params
        permitted_params = [:email, :first_name, :last_name, :phone_number, :company_id, :external_id]

        unless params[:user][:password].blank?
          permitted_params += [:password, :password_confirmation]
        end

        user_params = params.require(:user).send(:permit, permitted_params)
      end
  end
end

class MeetingRequestsController < ApplicationController
  def index
    @company = current_user.company
    @incoming_meeting_requests = @company.incoming_meeting_requests
    @outgoing_meeting_requests = @company.outgoing_meeting_requests
  end

  def accept
    @company = current_user.company
    @meeting_request = @company.incoming_meeting_requests.pending.find(params[:id])

    if @meeting_request.nil?
      redirect_to meeting_requests_url, alert: I18n.t('meeting_requests.accept.not_found')
    elsif @meeting_request.pending?
      @meeting_request.accept
      redirect_to meeting_requests_url, notice: I18n.t('meeting_requests.accept.accepted')
    else
      redirect_to meeting_requests_url, alert: I18n.t('meeting_requests.accept.could_not_be_accepted')
    end
  end

  def new
    @company = current_user.company
    @target_company = Company.find params[:company_id]
    @meeting_request = @company.outgoing_meeting_requests.pending.build
  end

  def create
    @company = current_user.company
    @target_company = Company.find params[:company_id]
    @meeting_request = @company.outgoing_meeting_requests.pending.build requestee_company: @target_company

    respond_to do |format|
      unless MeetingService.can_request_meeting(@company, @target_company)
        @meeting_request.errors.add :base, I18n.t('meeting_requests.create.cannot_be_created')

        format.html { render action: 'new' }
      else
        if @meeting_request.save
          if @target_company.anchor?
            MeetingRequestMailer.created(@meeting_request.id).deliver
          else
            @meeting_request.accept # meeting requests for non-anchors are auto-accepted
          end

          format.html { redirect_to meeting_requests_path, notice: I18n.t('meeting_requests.create.successfully_created') }
        else
          format.html { render action: 'new' }
        end
      end
    end
  end

  private
    def meeting_request_params
      params.require(:meeting_request).permit(:requester_company_id, :requestee_company_id, :status)
    end
end

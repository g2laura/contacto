class DashboardController < ApplicationController
  def show
    unless current_user.admin?
      redirect_to current_user.company
    end
  end
end

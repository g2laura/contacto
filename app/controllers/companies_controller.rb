class CompaniesController < ApplicationController
  before_action :set_company, only: [:show, :edit, :update]

  # GET /companies
  def index
    @sectors = Sector.all
    @company = current_user.company
    if (params[:sector_id])
      @companies = Company.where(:sector_id => params[:sector_id]).order('name asc').page(params[:page]).per(6) 
      @sector = Sector.find(params[:sector_id])     
    elsif (params[:company_name])
      @companies = Company.where("name ilike '%#{params[:company_name]}%'").order('name asc').page(params[:page]).per(6)
    else
      @companies = Company.all.page(params[:page]).order('name asc').per(6)
    end
  end

  # GET /companies/1
  def show
  end

  # GET /companies/1/edit
  def edit
    @user = @company.contact_person
    respond_to do |format|
      format.js {}
    end
  end

  # PATCH/PUT /companies/1
  def update
    respond_to do |format|
      if @company.update(company_params)
        @user = @company.contact_person
        @user.update(user_params)
        format.html { redirect_to @company, notice: 'Sus datos se han actualizado exitosamente.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:name, :summary, :services, :needs, :offers, :sector_id, :website, :twitter, :logo)
    end
    
    def user_params
      if !params[:user].nil?
        params[:user].permit(:first_name, :last_name, :phone_number, :email, :mobile_phone)
      end
    end
end

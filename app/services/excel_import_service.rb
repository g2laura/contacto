class ExcelImportService

  @@header_definition = {
    'e-mail'     => :user_email,
    'password'   => :user_password,
    'codigo'     => :external_id,
    'celular'    => :user_mobile_phone,
    'nombre persona contacto'   => :user_first_name,
    'apellido persona contacto' => :user_last_name,

    'sector' => :sector_name,

    'empresa'      => :company_name,
    'telefono'     => :company_phone_number,
    'web'          => :company_website,
    'twitter'      => :company_twitter,
    'que busca'    => :company_needs,
    'que ofrece'   => :company_offers,
    'patrocinante' => :company_sponsor,
    'presentacion de la empresa' => :company_summary,
    'productos y servicios'      => :company_services,
  }

  def initialize(filename)
    @filename = filename
  end

  def open_file
    @workbook = RubyXL::Parser.parse(@filename)
  end

  def get_or_create_user_from(attributes)
    logger.info "user '#{attributes[:user_email]}' with password '#{attributes[:user_password]}'"

    User.where(external_id: attributes[:external_id]).first_or_create! do |user|
      user.email        = attributes[:user_email].downcase
      user.password     = attributes[:user_password]
      user.mobile_phone = attributes[:user_mobile_phone]
      user.first_name   = attributes[:user_first_name]
      user.last_name    = attributes[:user_last_name]
    end
  end

  def get_or_create_company_from(attributes)
    Company.where(external_id: attributes[:external_id]).first_or_create! do |company|
      company.phone_number = attributes[:company_phone_number]
      company.name     = attributes[:company_name]
      company.website  = attributes[:company_website]
      company.twitter  = attributes[:company_twitter]
      company.summary  = attributes[:company_summary]
      company.services = attributes[:company_services]
      company.needs    = attributes[:company_needs]
      company.offers   = attributes[:company_offers]
      company.sponsor  = /si/i.match(attributes[:company_sponsor] || '') != nil
    end
  end

  def get_or_create_sector_from(attributes)
    Sector.where(name: attributes[:sector_name]).first_or_create
  end

  def get_or_create_objects_from(attributes)
    user    = get_or_create_user_from attributes
    company = get_or_create_company_from attributes
    sector  = get_or_create_sector_from attributes

    user.update! company: company
    company.update! contact_person: user, sector: sector

    logger.debug user.inspect
    logger.debug company.inspect
  end

  def get_worksheet
    worksheets = @workbook.worksheets.find_all { |w| w.sheet_name.downcase =~ /web/ }

    if worksheets.count < 1
      logger.fatal "could not find WEB worksheet on: #{worksheets.collect(&:sheet_name)}"
      raise "could not find WEB worksheet on: #{worksheets.collect(&:sheet_name)}"
    end

    worksheets.first
  end

  def import
    open_file
    worksheet = get_worksheet
    data = worksheet.extract_data

    logger.debug "searching for header row on #{data.count} row(s)"

    header_row = nil
    data.count.times do |row|
      first  = data[row][1] || ''
      second = data[row][2] || ''

      first  = I18n.transliterate(first.to_s).downcase
      second = I18n.transliterate(second.to_s).downcase

      if first == 'password' && second == 'codigo'
        logger.debug "found header row on row #{row}"
        header_row = row
        break
      end
    end

    header_index = {}

    if header_row.nil?
      logger.fatal 'could not find header row'
      raise 'could not find header row'
    else
      logger.debug 'generating header index'

      data[header_row].each_with_index do |header_cell, index|
        header = header_cell || ''
        header = I18n.transliterate(header.to_s).downcase

        unless header.empty?
          header_index[header] = index
        end
      end

      logger.info "header index generated: #{header_index.inspect}"

      row = header_row + 1
      end_of_table_reached = false

      data.count.times.drop(header_row + 1).each do |row|
        attributes = {}
        @@header_definition.each do |name, field|
          logger.debug "#{name} = #{header_index[name]} | #{field} = #{data[row][header_index[name]]}"
          attributes[field] = data[row][header_index[name]]
        end

        unless attributes[:user_email]
          logger.warn "[WARNING]: skipping record #{row} since 'email' field is empty"
        else
          if attributes[:external_id].nil? || attributes[:external_id].empty?
            logger.warn "[WARNING]: skipping record #{row} since 'codigo' field is empty"
          else
            get_or_create_objects_from attributes
          end
        end
      end
    end
  end

  def logger
    Rails.logger
  end
end

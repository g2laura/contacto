class MeetingService
  def self.common_available_spots(requester, requestee)
    spots = requester.all_available_meeting_spots & requestee.all_available_meeting_spots
    spots.sort { |a, b| a[:begin_at] <=> b[:begin_at] }
  end

  def self.available_tables_for(spot)
    all_tables - Meeting.spot(spot).pluck(:table_number)
  end

  def self.arrange_meeting(requester, requestee)
    spots  = common_available_spots requester, requestee

    selected_spot  = nil
    selected_table = nil

    spots.each do |spot|
      tables = available_tables_for(spot)

      if tables.count > 0
        selected_spot  = spot
        selected_table = tables[0]
        break
      end
    end

    if selected_spot
      meeting = Meeting.new requester_company: requester, company: requestee
      meeting.begin_at = selected_spot[:begin_at]
      meeting.end_at   = selected_spot[:end_at]
      meeting.table_number = selected_table
      meeting
    end
  end

  def self.all_tables
    (1..Meeting::MAX_TABLE_NUMBER).to_a
  end

  def self.can_request_meeting(requester, requestee)
    Time.now < requester.max_time_limit &&
    requester != requestee &&
    Meeting.where(requester_company: requester).where(company: requestee).count == 0 &&
    Meeting.where(requester_company: requestee).where(company: requester).count == 0 &&
    MeetingRequest.where(requester_company: requester).where(requestee_company: requestee).count == 0 &&
    MeetingRequest.where(requester_company: requestee).where(requestee_company: requester).count == 0 &&
    requester.all_available_meeting_spots.count > 0 &&
    requestee.all_available_meeting_spots.count > 0 &&
    requester.outgoing_meeting_requests.active.count < requester.max_outgoing_requests &&
    requestee.incoming_meeting_requests.active.count < requestee.max_incoming_requests
  end

  def self.can_accept_meeting_request(meeting_request, company)
    meeting_request.requestee_company == company && meeting_request.status == :pending
  end

  def self.can_reject_meeting_request(meeting_request, company)
    meeting_request.requestee_company == company && meeting_request.status == :pending
  end
end

module ApplicationHelper
  def status_label(status)
    class_names = ['label']

    case status
    when :pending
      # none
    when :accepted
      class_names << 'label-info'
    when :scheduled
      class_names << 'label-success'
    when :failed
      class_names << 'label-important'
    else
      class_names << 'label-inverse'
    end

    content_tag 'span', t("meeting_requests.status.#{status}"), class: class_names.join(' ')
  end

  def display_new_meeting_request_link_for?(requestee)
    requester = current_user.company

    Time.now < current_user.company.max_time_limit &&
    requester != requestee &&
    requester.all_available_meeting_spots.count > 0 &&
    !requester.has_meeting_with(requestee)
  end
end

class ScheduleDocument < Prawn::Document
  def initialize(company)
    super()
    @company = company

    company_info
    schedule_table
  end

  def company_info
    image "#{Rails.root}/app/assets/images/logo-contacto.png"

    text @company.name, size: 30, style: :bold
    text @company.sector.name
    text @company.contact_person.full_name
  end

  def schedule_table
    move_down 20

    text 'Agenda Matching', size: 20, style: :bold

    rows = initial_activities_rows +
           [[{ content: "Bloque 1 - Matching", colspan: @company.max_meetings_per_spot + 1 }]] +
           meeting_rows(:first_block) +
           workshow_1 +
           [[{ content: "Bloque 2 - Matching", colspan: @company.max_meetings_per_spot + 1 }]] +
           meeting_rows(:second_block) +
           workshow_2 +
           [[{ content: "Bloque 3 - Matching", colspan: @company.max_meetings_per_spot + 1 }]] +
           meeting_rows(:third_block) +
           closing

    table rows.unshift(meeting_headers) do
      row(0).font_style    = :bold
      column(0).font_style = :bold

      row(0).align    = :center
      column(0).align = :center

      self.header = true
    end
  end

  def initial_activities_rows
    [
      [
        { content: '07:30 AM', font_style: :bold },
        { content: 'Registro de los participantes', font_style: :bold, colspan: @company.max_meetings_per_spot }
      ],
      [
        { content: '08:30 AM', font_style: :bold },
        { content: 'Inauguración, palabras de bienvenida.', font_style: :bold, colspan: @company.max_meetings_per_spot }
      ]
    ]
  end

  def workshow_1
    [
      [
        { content: '10:00 AM', font_style: :bold },
        { content: 'Workshop 1: Hacer empresa en tiempos de crisis', font_style: :bold, colspan: @company.max_meetings_per_spot }
      ]
    ]
  end

  def workshow_2
    [
      [
        { content: '12:30 PM', font_style: :bold },
        { content: 'Almuerzo', font_style: :bold, colspan: @company.max_meetings_per_spot }
      ],
      [
        { content: '02:30 PM', font_style: :bold },
        { content: 'Workshop 2: Retos y oportunidades para la empresa en Venezuela', font_style: :bold, colspan: @company.max_meetings_per_spot }
      ]
    ]
  end

  def closing
    [
      [
        { content: '05:30 PM', font_style: :bold },
        { content: 'Clausura', font_style: :bold, colspan: @company.max_meetings_per_spot }
      ]
    ]
  end

  def meeting_headers
    headers =  [ "Hora" ]

    if @company.sponsor?
      headers += [ "1er Encuentro" ]
      headers += [ "2do Encuentro" ]
    else
      headers += [ "Encuentro" ]
    end

    headers
  end

  def meeting_rows(block_number)
    Meeting.spots(block_number).map do |spot|
      begin_at = I18n.l(spot[:begin_at], format: :time_of_day)
      end_at   = I18n.l(spot[:end_at], format: :time_of_day)

      meetings = @company.meetings.spot(spot).map do |meeting|
        "#{meeting.other_company(@company)}\nMesa #{meeting.table_number}"
      end

      ["#{begin_at}\n#{end_at}", meetings].flatten
    end

  end
end

class AssignmentDocument < Prawn::Document
  def initialize
    super

    header_info

    font_size 8
    MeetingService.all_tables.each_slice(3) do |tables|
      assignment_table(tables)
      start_new_page
    end
  end

  def header_info
    image "#{Rails.root}/app/assets/images/logo-contacto.png"

    move_down 20
    text 'Asignación de Mesas', size: 20, style: :bold
  end

  def assignment_table(tables)
    rows = [ ["Horas /\nMesas"] + tables ] + meetings_by_hour(tables)
    table rows
  end

  def meetings_by_hour(tables)
    Meeting.all_spots.map do |spot|
      begin_at = I18n.l(spot[:begin_at], format: :time_of_day)
      [ begin_at ] +
      tables.map do |table_number|
        m = Meeting.spot(spot).on_table(table_number).first
        if m
          "#{m.requester_company.name} /\n#{m.company.name}"
        else
          ""
        end
      end
    end
  end
end

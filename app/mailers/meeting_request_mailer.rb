class MeetingRequestMailer < ActionMailer::Base
  include Resque::Mailer

  default from: "info@cdo.org.ve"
  layout 'email'

  def created(meeting_request, options = {})
    meeting_request = MeetingRequest.find meeting_request unless options[:preview]

    @meeting_request = meeting_request
    @requester = @meeting_request.requester_company
    @requestee = @meeting_request.requestee_company

    mail to: @requestee.contact_person.email
  end

  def scheduled(meeting, company, options = {})
    meeting = Meeting.find meeting unless options[:preview]
    company = Company.find company unless options[:preview]
    other_company = meeting.other_company company

    @meeting = meeting
    @company = company
    @other_company = other_company

    begin_at = Time.parse("2013-07-02 #{meeting.begin_at.strftime('%I:%M:%S')} -04:30").utc
    end_at   = begin_at + 15.minutes

    event = RiCal.Event do
      summary     "Encuentro con #{other_company.name}"
      description "Encuentro con #{other_company.name}"
      dtstart     begin_at
      dtend       end_at
      location    "Contacto Venezuela - Mesa #{meeting.table_number}"
      add_attendee company.contact_person.email
      add_attendee other_company.contact_person.email
    end
    attachments['meeting.ics'] = { mime_type: 'text/calendar', content: event.export } unless options[:preview]

    mail to: @company.contact_person.email
  end

  def pdf_schedule(company, options = {})
    company = Company.find company unless options[:preview]

    @company = company

    pdf = ScheduleDocument.new @company
    attachments['contacto-ve-agenda.pdf'] = { mime_type: 'application/pdf', content: pdf.render } unless options[:preview]
    mail to: @company.contact_person.email, cc: 'info@cdo.org.ve'
  end
end

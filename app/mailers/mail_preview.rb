class MailPreview < MailView

  def created
   meeting_request = MeetingRequest.new id: 1234, requester_company: requester_company, requestee_company: requestee_company

   MeetingRequestMailer.created(meeting_request, preview: true)
  end

  def participant_requester_scheduled
    meeting = Meeting.new id: 1234, requester_company: requester_company, company: requestee_company,
      begin_at: '09:00:00', end_at: '09:15:00', table_number: 18

    MeetingRequestMailer.scheduled(meeting, requester_company, preview: true)
  end

  def participant_requestee_scheduled
    meeting = Meeting.new id: 1234, requester_company: requester_company, company: requestee_company,
      begin_at: '09:00:00', end_at: '09:15:00', table_number: 18

    MeetingRequestMailer.scheduled(meeting, requestee_company, preview: true)
  end

  def anchor_requestee_scheduled
    meeting = Meeting.new id: 1234, requester_company: requester_company, company: requestee_company,
      begin_at: '09:00:00', end_at: '09:15:00', table_number: 18

    requestee_company.anchor = true

    MeetingRequestMailer.scheduled(meeting, requestee_company, preview: true)
  end

  def pdf_schedule
    MeetingRequestMailer.pdf_schedule(requester_company, preview: true)
  end

  protected
  def requester_contact_person
    @requester_contact_person ||= User.new first_name: 'Carlos', last_name: 'Alonzo', email: 'calonzo@divux.com',
      mobile_phone: '0414-123.45.67'
  end

  def requestee_contact_person
    @requestee_contact_person ||= User.new first_name: 'John', last_name: 'Doe', email: 'john.doe@divux.com',
      mobile_phone: '0412-765.43.21'
  end

  def requester_company
    @requester_company ||= Company.new id: 1234, name: 'Divux Systems, C.A.', contact_person: requester_contact_person
  end

  def requestee_company
    @requestee_company ||= Company.new id: 1235, name: 'Seguros Caracas', contact_person: requestee_contact_person
  end
end
 
class Company < ActiveRecord::Base
  belongs_to :sector
  belongs_to :contact_person, class_name: User.to_s
  
  mount_uploader :logo, ImageUploader

  has_many :incoming_meeting_requests, class_name: MeetingRequest.to_s, foreign_key: :requestee_company_id
  has_many :outgoing_meeting_requests, class_name: MeetingRequest.to_s, foreign_key: :requester_company_id

  scope :sponsors, -> { where(sponsor: true) }
  scope :anchors,  -> { where(anchor: true) }
  scope :search,   ->(name) { t = Company.arel_table; where t[:name].matches("%#{name}%") }

  def has_meeting_with(company)
    t = Meeting.arel_table
    requested_by_us   = t.grouping(t[:requester_company_id].eq(id).and(t[:company_id].eq(company.id)))
    requested_by_them = t.grouping(t[:requester_company_id].eq(company.id).and(t[:company_id].eq(id)))

    Meeting.where(requested_by_us.or(requested_by_them)).count > 0
  end

  def meetings
    t = Meeting.arel_table
    Meeting.where(t[:requester_company_id].eq(id).or(t[:company_id].eq(id)))
  end

  def confirmed_meetings
    meetings.first_block | meetings.second_block | meetings.third_block
  end

  def all_available_meeting_spots
    available_meeting_spots(:first_block) + available_meeting_spots(:second_block) + available_meeting_spots(:third_block)
  end

  def available_meeting_spots(block_number)
    block_spots = Meeting.spots(block_number)

    if sponsor
      block_spots *= 2 # duplicate available spots
      block_spots.sort! { |x,y| x[:begin_at] <=> y[:begin_at] }
    end

    booked_spots = meetings.send(block_number).pluck(:begin_at)
    block_spots.reject do |spot|
      index = booked_spots.index spot[:begin_at]

      booked_spots.delete_at index if index
      index
    end
  end

  def max_meetings_per_spot
    sponsor? ? 2 : 1
  end

  def max_meetings
    @max_meetings ||= Meeting.all_spots.count * max_meetings_per_spot
  end

  def max_incoming_requests
    if self[:incoming_request_limit] < 0
      max_meetings / 2
    else
      self[:incoming_request_limit]
    end
  end

  def max_outgoing_requests
    if self[:outgoing_request_limit] < 0
      max_meetings / 2
    else
      self[:outgoing_request_limit]
    end
  end

  def max_time_limit
    time_limit.blank? ? Time.parse("2013-07-01 15:01:00 -0430") : time_limit
  end

  def to_s
    name
  end
end

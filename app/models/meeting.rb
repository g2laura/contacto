class Meeting < ActiveRecord::Base
  belongs_to :requester_company, class_name: Company.to_s
  belongs_to :company

  scope :first_block,  -> { where(begin_at: "09:00:00".."10:00:00") }
  scope :second_block, -> { where(begin_at: "11:30:00".."12:30:00") }
  scope :third_block,  -> { where(begin_at: "15:30:00".."17:30:00") }
  scope :spot, ->(spot) { where(begin_at: spot[:begin_at]) }
  scope :on_table, ->(table_number) { where(table_number: table_number) }

  validates_uniqueness_of :company, scope: :requester_company

  MAX_TABLE_NUMBER = 47

  def self.all_spots
    spots(:first_block) + spots(:second_block) + spots(:third_block)
  end

  def self.spots(block_number)
    case block_number
    when :first_block
      block_begin = Time.parse("2000-01-01 09:00:00 UTC")
      block_end   = block_begin + 1.hour
    when :second_block
      block_begin = Time.parse("2000-01-01 11:30:00 UTC")
      block_end   = block_begin + 1.hour
    when :third_block
      block_begin = Time.parse("2000-01-01 15:30:00 UTC")
      block_end   = block_begin + 2.hour
    else
      return []
    end

    spot_duration = 15.minutes
    spot_break    = 5.minutes

    block_spots = []

    current_spot = block_begin

    while current_spot < block_end
      spot_begin = current_spot
      spot_end   = spot_begin + spot_duration

      block_spots.push({ begin_at: spot_begin, end_at: spot_end })

      current_spot = spot_end + spot_break
    end

    block_spots
  end

  def other_company(us)
    if us == requester_company
      them = company
    else
      them = requester_company
    end

    them
  end
end

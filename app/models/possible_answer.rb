class PossibleAnswer < ActiveRecord::Base
  belongs_to :question
  has_many   :answers

  accepts_nested_attributes_for :answers
end

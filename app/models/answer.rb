class Answer < ActiveRecord::Base
  belongs_to :user
  belongs_to :question
  belongs_to :possible_answer

  scope :by_question, ->(question) { where(question_id: question.id) }
end

class Question < ActiveRecord::Base
  belongs_to :survey
  has_many   :answers
  has_many   :possible_answers

  accepts_nested_attributes_for :answers
  accepts_nested_attributes_for :possible_answers
end

class MeetingRequest < ActiveRecord::Base
  belongs_to :requester_company, class_name: Company.to_s
  belongs_to :requestee_company, class_name: Company.to_s

  default_scope -> { order(:created_at) }

  scope :pending,   -> { where(status: :pending) }
  scope :scheduled, -> { where(status: :scheduled) }

  scope :active, -> { where(status: [:pending, :scheduled]) }

  validates_uniqueness_of :requestee_company, scope: :requester_company

  def meeting
    Meeting.where(requester_company: requester_company, company: requestee_company).first
  end

  def accept
    Resque.enqueue(MeetingArranger, self[:id])
    update status: :accepted
  end

  def moderate
    update status: :pending_moderation
  end

  def scheduled(meeting)
    update status: :scheduled
  end

  def failed
    update status: :failed
  end

  def pending?
    status == :pending
  end

  def status
    if self[:status]
      self[:status].to_sym
    else
      self[:status]
    end
  end

  def other_company(us)
    @other_company ||= us == requester_company ? requestee_company : requester_company
  end
end

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :company
  has_many   :answers

  default_scope -> { order(:first_name) }
  scope :admins,   -> { where(admin: true) }
  scope :sponsors, -> { joins(:company).where(companies: { sponsor: true }) }
  scope :anchors,  -> { joins(:company).where(companies: { anchor: true }) }

  def full_name
    "#{first_name} #{last_name}"
  end

  def to_s
    full_name
  end
end

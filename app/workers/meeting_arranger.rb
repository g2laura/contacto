class MeetingArranger
  @queue = :meetings_queue
  
  def self.perform(meeting_request_id)
    meeting_request = MeetingRequest.find(meeting_request_id)
    requester = meeting_request.requester_company
    requestee = meeting_request.requestee_company

    meeting = MeetingService.arrange_meeting requester, requestee

    if meeting && meeting.save
      meeting_request.scheduled meeting

      [:requester_company, :company].each do |company_kind|
        MeetingRequestMailer.scheduled(meeting.id, meeting.send(company_kind).id).deliver
      end
    else
      meeting_request.failed
    end
  end
end

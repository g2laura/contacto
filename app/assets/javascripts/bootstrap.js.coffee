jQuery ->
  $("a[rel=popover]").popover()
  $(".tooltip").tooltip()
  $("a[rel=tooltip]").tooltip()
  $(document).on 'click', '#edit_company_summary', -> # Show form to edit a company summary
    $("#info_company_summary").hide()
    $(".company-summary").show()
  $(document).on 'click', '#edit_company_services', -> # Show form to edit a company services
    $("#info_company_services").hide()
    $(".company-services").show()
  $(document).on 'click', '#edit_company_needs', -> # Show form to edit a company needs
    $("#info_company_needs").hide()
    $(".company-needs").show()
  $(document).on 'click', '#edit_company_offers', -> # Show form to edit a company offers
    $("#info_company_offers").hide()
    $(".company-offers").show()   
require "bundler/capistrano"

set :application, "contactove"
set :repository,  "git@git.divux.com:cdo/contacto-ve-web-app.git"

server "app.contactove.cdo.org.ve", :web, :app, :db, :primary => true

set :deploy_to,  "/srv/apps/#{application}"
set :deploy_via, :rsync_with_remote_cache

set :user, "ubuntu"
set :use_sudo, false

# CarrierWave uploads
set :shared_children, shared_children + %w{public/uploads}

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# Restart Resque Workers
after "deploy:symlink", "resque:restart"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end

namespace :resque do
  task :start do
    run "cd #{current_path} && #{try_sudo} #{rake} resque:work QUEUE='*' PIDFILE=tmp/pids/resque.pid BACKGROUND=yes RAILS_ENV=production"
  end

  task :stop do
    run "cd #{current_path} && #{try_sudo} kill $(cat tmp/pids/resque.pid) || echo 'stop failed'"
  end

  task :restart do
    resque.stop
    resque.start
  end
end


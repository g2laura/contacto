ContactoVeWebApp::Application.routes.draw do
  resources :surveys, :only => [:index, :edit, :update]

  resources :companies, :only => [:index, :show, :edit, :update] do
    resources :meeting_requests, only: [ :new, :create ]
  end

  resources :meetings, only: :index
  resources :meeting_requests, only: :index do
    member do
      get :accept
      get :reject
    end
  end

  devise_for :users

  namespace :admin do
    resources :users do
      collection do
        get :admins
        get :sponsors
        get :anchors
      end
    end

    resources :meetings
    resources :meeting_requests do
      member do
        get :notify
      end
    end
    resources :companies do
      collection do
        get :sponsors
        get :anchors
      end

      member do
        get :schedule
        get :send_schedule
      end
    end

    resources :surveys, :only => [:index, :edit, :update]

    get 'survey_users',     to: 'surveys#users'
    get 'survey_companies', to: 'surveys#companies'
    get 'survey_user/:id',  to: 'surveys#user', as: 'survey_user'

    authenticate :user, lambda { |u| u.admin? } do
      mount Resque::Server, :at => "/resque"
      mount MailPreview,    :at => "/mail_view"
    end
  end

  root 'dashboard#show'
end
